/*
* Author: Jaime Rivera
* Date : 2020.03.21
* Revision : 2020.04.11
* Copyright : Copyright 2020 Jaime Rivera
* Brief: Basic implementation of Conway�s "Life", using the stb library to display the output/result
* Credits: Sean Barrett, author of the STB library, used in this project (https://github.com/nothings/stb)
*/


#include <iostream>
#include <random>
#include <string>
#include <direct.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>


// --------- CONSTANTS --------- //

// OUTPUT DIR
std::string OUTPUT_FOLDER = "output_frames";
const char *out_folder = OUTPUT_FOLDER.c_str();

// OUTPUT PARAMETERS
const int FRAMES_TO_WRITE = 300;
const int WIDTH = 192;
const int HEIGHT = 108;
const short CHANNELS = 4;


// --------- CELL ARRAYS --------- //
bool old_state[WIDTH][HEIGHT];
bool new_state[WIDTH][HEIGHT];
bool get_cell(int x, int y);


// --------- MAIN --------- //
int main()
{

	// GENERATOR AND ARRAY POPULATION
	std::default_random_engine generator;
	std::uniform_int_distribution<int> random_bool(0, 1);

	for (int j = 0; j < HEIGHT; j++)
	{
		for (int i = 0; i < WIDTH; i++)
		{
			bool val = random_bool(generator);
			new_state[i][j] = val;
		}
	}

	// FRAME WRITING
	int mkdir_result = _mkdir(out_folder);
	for (int frame = -50; frame < FRAMES_TO_WRITE; frame++)
	{
		// Storing the old values
		for (int y = 0; y < HEIGHT; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				old_state[x][y] = new_state[x][y];
			}
		}

		// Calculating new values
		for (int y = 0; y < HEIGHT; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				int total_neighbors = get_cell(x - 1, y - 1) + get_cell(x + 0, y - 1) + get_cell(x + 1, y - 1) +
					                  get_cell(x - 1, y + 0) +            0           + get_cell(x + 1, y + 0) +
					                  get_cell(x - 1, y + 1) + get_cell(x + 0, y + 1) + get_cell(x + 1, y + 1);

				if (get_cell(x, y))
					new_state[x][y] = total_neighbors == 2 || total_neighbors == 3;
				else
					new_state[x][y] = total_neighbors == 3;
			}
		}

		// Pixel colors
		unsigned char *pixels = new unsigned char[WIDTH * HEIGHT * CHANNELS];

		int index = 0;
		for (int j = 0; j < HEIGHT; j++)
		{
			for (int i = 0; i < WIDTH; i++)
			{
				bool state = old_state[i][j];

				int color;
				color = state == true ? 255 : 0;
				pixels[index++] = color; // R
				pixels[index++] = color; // G
				pixels[index++] = color; // B
				pixels[index++] = color; // Alpha
			}
		}


		// Writing the output file
		if (frame < 0) { continue; }
		const std::string out_filename_string = OUTPUT_FOLDER + "/life_" + std::to_string(frame) + ".png";
		const char *out_filename = out_filename_string.c_str();
		stbi_write_png(out_filename, WIDTH, HEIGHT, CHANNELS, pixels, WIDTH * CHANNELS);
		delete[] pixels;

	}

	return 0;
}


// --------- AUX FUNCTIONS --------- //
bool get_cell(int x, int y)
{
	if (x >= 0 && x <= WIDTH && y >= 0 && y <= HEIGHT)
	{
		return old_state[x][y];
	}

	return false;
}