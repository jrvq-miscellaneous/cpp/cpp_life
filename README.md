# Conway's "Life" (C++)

A quick implementation of John Conway's "Game of Life" for C++.
The results of the different generations have their output as png images, so they can be post-processed for a better-looking presentation.

### Output example
This is one of the produced images, after a bit of post-processing done in Nuke.
![cpp_life](http://www.jaimervq.com/wp-content/uploads/2020/03/life_example.png)

### Credits
Credits to Sean Barret for his STB library, which is being used in this project for the creation of the png images.
Check it out at: https://github.com/nothings/stb

***

For more info: www.jaimervq.com